package lt.vitalikas;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

public class Solution {
    public static void main(String[] args) {
        System.out.print("Enter string: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        StringBuilder binaryFinal = new StringBuilder();
        StringBuilder solution = new StringBuilder();

        input.chars().forEach(ch -> {
            String binary = Integer.toBinaryString(ch);
            if (binary.length() == 6) binaryFinal.append("0").append(binary);
            else binaryFinal.append(binary);
        });
        System.out.println(binaryFinal);

        AtomicReference<Character> previousBinValue = new AtomicReference<>('_');
        StringBuilder unaryOfZero = new StringBuilder("00 0");
        StringBuilder unaryOfOne = new StringBuilder("0 0");

        binaryFinal.chars().forEach(ch -> {
            if ((char)ch == previousBinValue.get()) solution.append("0");
            else {
                if (previousBinValue.get() == '_') {
                    if (((char)ch) == '0') solution.append(unaryOfZero);
                    else solution.append(unaryOfOne);
                } else {
                    if (((char)ch) == '0') solution.append(" ").append(unaryOfZero);
                    else solution.append(" ").append(unaryOfOne);
                }
            }
            previousBinValue.set((char) ch);
        });
        System.out.println(solution);
        scanner.close();
    }
}
